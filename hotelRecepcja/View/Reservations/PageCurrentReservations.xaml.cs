﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MySql.Data.MySqlClient;

namespace hotelRecepcja.View.Reservations
{
    /// <summary>
    /// Logika interakcji dla klasy PageCurrentReservations.xaml
    /// </summary>
    public partial class PageCurrentReservations : Page
    {
        private PageReservationsView _parent;
        public PageCurrentReservations(PageReservationsView p)
        {
            _parent = p;
            InitializeComponent();
        }

        public void Refresh()
        {
            GetData();
        }

        private void GetData()
        {
            try
            {
                using (var con = new MySqlConnection(Properties.Settings.Default.ConnectionString))
                {
                    using (var cmd = new MySqlCommand("GetCurrentlyOccupiedRooms", con))
                    {
                        con.Open();
                        cmd.CommandType = CommandType.StoredProcedure;
                        var r = cmd.ExecuteReader();
                        var dt = new DataTable();
                        dt.Load(r);
                        DataGridShowReservations.DataContext = null;
                        DataGridShowReservations.DataContext = dt;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex);
            }
        }
    }
}
