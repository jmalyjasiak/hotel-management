﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MySql.Data.MySqlClient;

namespace hotelRecepcja.Reservations
{
    /// <summary>
    /// Interaction logic for PageReservationsToConfirm.xaml
    /// </summary>
    public partial class PageReservationsToConfirm : Page
    {
        private PageReservationsView _parent;
        public PageReservationsToConfirm(PageReservationsView parent)
        {
            _parent = parent;
            InitializeComponent();
            GetData();
        }

        public void Refresh()
        {
            GetData();

        }

        public void GetData()
        {
            try
            {
                using (var con = new MySqlConnection(Properties.Settings.Default.ConnectionString))
                {
                    using (var cmd = new MySqlCommand("Select * from reservations_to_confirm", con))
                    {
                        con.Open();
                        DataTable dt = new DataTable();
                        dt.Load(cmd.ExecuteReader());
                        DataGridShowReservations.DataContext = dt;
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Błąd połączenia " + e);
            }
        }
        private void TextBoxSearch_OnGotFocus(object sender, RoutedEventArgs e)
        {
            var tb = e.Source as TextBox;
            if (tb == null) return;
            if (tb.Text.Equals(tb.ToolTip.ToString()))
                tb.Text = "";

        }

        private void TextBoxSearch_OnLostFocus(object sender, RoutedEventArgs e)
        {
            var tb = e.Source as TextBox;
            if (tb == null) return;
            if (string.IsNullOrWhiteSpace(tb.Text))
                tb.Text = tb.ToolTip.ToString();
        }

        private string ParseDate(DateTime d)
        {
            var sb = new StringBuilder();
            sb.Append(d.Year + "-");
            sb.Append(d.Month + "-");
            sb.Append(d.Day);
            return sb.ToString();
        }

        private void Search()
        {
            var command = new StringBuilder();
            command.Append("select * from reservations_to_confirm ");

            var sbArgs = new StringBuilder();
            if (!TextBoxFirstName.Text.Equals(TextBoxFirstName.ToolTip))
                sbArgs.Append($"Imie = '{TextBoxFirstName.Text}'");

            if (!TextBoxLastName.Text.Equals(TextBoxLastName.ToolTip))
            {
                if (sbArgs.Length > 0)
                    sbArgs.Append(" and ");
                sbArgs.Append($"Nazwisko = '{TextBoxLastName.Text}'");
            }
            if (!TextBoxPhone.Text.Equals(TextBoxPhone.ToolTip))
            {
                if (sbArgs.Length > 0)
                    sbArgs.Append(" and ");
                sbArgs.Append($"Telefon = '{TextBoxPhone.Text}'");
            }
            if (!TextBoxRoom.Text.Equals(TextBoxRoom.ToolTip))
            {
                if (sbArgs.Length > 0)
                    sbArgs.Append(" and ");
                sbArgs.Append($"Pokoje LIKE '%{TextBoxRoom.Text}%'");
            }
            if (DatePickerStart.SelectedDate != null)
            {
                var dateStart = ParseDate(DatePickerStart.SelectedDate.Value);
                if (sbArgs.Length > 0)
                    sbArgs.Append(" and ");
                sbArgs.Append($"Koniec >= CAST('{dateStart}' AS DATE)");
            }
            if (DatePickerEnd.SelectedDate != null)
            {
                var dateEnd = ParseDate(DatePickerEnd.SelectedDate.Value);
                if (sbArgs.Length > 0)
                    sbArgs.Append(" and ");
                sbArgs.Append($"Poczatek <= CAST('{dateEnd}' AS DATE)");
            }
            if (sbArgs.Length > 0)
                command.Append($"where {sbArgs.ToString()};");


            try
            {
                using (var con = new MySqlConnection(Properties.Settings.Default.ConnectionString))
                {
                    using (var cmd = new MySqlCommand(command.ToString(), con))
                    {
                        con.Open();
                        DataTable dt = new DataTable();
                        dt.Load(cmd.ExecuteReader());
                        DataGridShowReservations.DataContext = null;
                        DataGridShowReservations.DataContext = dt;
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Błąd połączenia " + e);
            }
        }

        private void ButtonSearch_OnClick(object sender, RoutedEventArgs e)
        {
            Search();
        }

        private int GetSelectedReservationId()
        {
            var row = (DataRowView)DataGridShowReservations.SelectedItem;
            int reservation_id;
            try
            {
                reservation_id = Convert.ToInt32(row.Row.ItemArray[0].ToString());
            }
            catch (Exception e)
            {
                reservation_id = -1;
            }
            return reservation_id;
        }
        private void MenuItemDelete_OnClick(object sender, RoutedEventArgs e)
        {
            int id = GetSelectedReservationId();
            if (id == -1) return;
            try
            {
                using (var con = new MySqlConnection(Properties.Settings.Default.ConnectionString))
                {

                    using (var cmd = new MySqlCommand("delete from reservations where reservations.id_reservation=@id", con))
                    {
                        cmd.Parameters.AddWithValue("@id", id);
                        con.Open();
                        var result = cmd.ExecuteNonQuery();
                        MessageBox.Show((result == 0) ? "Operacja nie powiodła się" : "Pomyślnie usunięto rezerwację");
                        GetData();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd połączenia " + e);
            }
        }

        private void MenuItemConfirm_OnClick(object sender, RoutedEventArgs e)
        {
            int id = GetSelectedReservationId();
            if (id == -1) return;
            try
            {
                using (var con = new MySqlConnection(Properties.Settings.Default.ConnectionString))
                {

                    using (var cmd = new MySqlCommand("ConfirmReservation", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@id", id);
                        cmd.Parameters["@id"].Direction = ParameterDirection.Input;
                        con.Open();
                        var result = cmd.ExecuteNonQuery();
                        MessageBox.Show((result == 0) ? "Operacja nie powiodła się" : "Pomyślnie potwierdzono rezerwację");
                        GetData();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd połączenia " + e);
            }
        }
    }
}