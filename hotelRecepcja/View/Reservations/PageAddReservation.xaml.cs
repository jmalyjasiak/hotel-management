﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using hotelRecepcja.Clients;
using hotelRecepcja.Rooms;
using MySql.Data.MySqlClient;

namespace hotelRecepcja.Reservations
{
    /// <summary>
    /// Interaction logic for PageAddReservation.xaml
    /// </summary>
    public partial class PageAddReservation : Page
    {
        private enum Modes { Add, Edit }

        private Modes _mode;
        private string _id;
        private PageShowClients _pageShowClients;
        private PageShowRooms _pageShowRooms;
        private PageReservationsView _parent;
        public PageAddReservation(PageReservationsView parent)
        {
            _parent = parent;
            InitializeComponent();

            _pageShowClients = new PageShowClients(null);
            _pageShowRooms = new PageShowRooms(null, true);
            Refresh();
        }

        public void Refresh()
        {
            ClearDatePickers();
            _pageShowClients.Refresh();
            _pageShowRooms.Refresh();


            FramePickClient.BorderBrush = new SolidColorBrush(Colors.Gray);
            FramePickClient.NavigationService.Navigate(_pageShowClients);
            FramePickRooms.BorderBrush = new SolidColorBrush(Colors.Gray);
            FramePickRooms.NavigationService.Navigate(_pageShowRooms);

            DatePickerStart.BorderBrush = new SolidColorBrush(Colors.Gray);
            DatePickerStart.SelectedDate = null;
            DatePickerEnd.BorderBrush = new SolidColorBrush(Colors.Gray);
            DatePickerEnd.SelectedDate = null;
        }

        private void ClearDatePickers()
        {
            DatePickerEnd.BlackoutDates.Clear();
            DatePickerStart.BlackoutDates.Clear();
            DatePickerEnd.BlackoutDates.AddDatesInPast();
            DatePickerStart.BlackoutDates.AddDatesInPast();
        }

        public void RunAddMode()
        {
            Refresh();
            _mode = Modes.Add;
        }

        public void RunEditMode(string id)
        {
            Refresh();
            _mode = Modes.Edit;
            _id = id;
            FillForm();
            UpdateBlackoutPeriods();

        }

        private void AddReservation()
        {
            if (
                MessageBox.Show(_parent.Parent, "Czy na pewno chcesz dodać rezerwację?", "Potwierdzenie",
                    MessageBoxButton.YesNo) == MessageBoxResult.No) return;

            if (!Validate()) return;

            var id = ExecuteAdd();
            ExecuteAddReservedRooms(id);

        }

        private string ParseDate(DateTime d)
        {
            var sb = new StringBuilder();
            sb.Append(d.Year+"-");
            sb.Append(d.Month + "-");
            sb.Append(d.Day);
            return sb.ToString();
        }

        private DateTime ConvertToDate(string d)
        {
            var dt = new DateTime();

            return dt;
        }

        private void FillForm()
        {
            string dateStart, dateEnd, idClient;
            using (var con = new MySqlConnection(Properties.Settings.Default.ConnectionString))
            {
                using (
                    var cmd =
                        new MySqlCommand($"select * from reservations where id_reservation={_id}", con))
                {
                    con.Open();
                    var reader = cmd.ExecuteReader();
                    if (!reader.Read()) return;
                    dateStart = reader["DATE_START"].ToString();
                    dateEnd = reader["DATE_END"].ToString();
                    idClient = reader["ID_CLIENT"].ToString();
                }
            }

            DatePickerStart.SelectedDate = DateTime.Parse(dateStart);
            DatePickerEnd.SelectedDate = DateTime.Parse(dateEnd);
            _pageShowClients.SelectClient(idClient);
            _pageShowRooms.SelectRooms(_id);
        }

        private string ExecuteAdd()
        {
            if (DatePickerStart.SelectedDate == null) return string.Empty;
            var dateStart = ParseDate(DatePickerStart.SelectedDate.Value);
            if (DatePickerEnd.SelectedDate == null) return string.Empty;
            var dateEnd = ParseDate(DatePickerEnd.SelectedDate.Value);
            var idClient = _pageShowClients.GetSelection();
            var command = "insert into reservations (date_start, date_end, id_client, confirmed)" +
                             $"values ('{dateStart}','{dateEnd}', '{idClient}','0')";
            try
            {
                using (var con = new MySqlConnection(Properties.Settings.Default.ConnectionString))
                {
                    using (var cmd = new MySqlCommand(command, con))
                    {
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Błąd połączenia " + e);
            }

            return GetReservationId(dateStart, dateEnd, idClient);
        }

        private static double GetCharge(string id)
        {
            using (var con = new MySqlConnection(Properties.Settings.Default.ConnectionString))
            {
                using (var cmd = new MySqlCommand("GetCharge", con))
                {
                    con.Open();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.Parameters["@id"].Direction = ParameterDirection.Input;
                    var r = cmd.ExecuteReader();
                    if (r.Read())
                    {
                       return r.GetDouble(0);
                    }
                }
            }
            return -1;
        }

        private void ExecuteAddReservedRooms(string id)
        {
            var rooms = _pageShowRooms.GetSelection();
            var result = 0;
            try
            {
                using (var con = new MySqlConnection(Properties.Settings.Default.ConnectionString))
                {
                    con.Open();
                    foreach (var room in rooms)
                    {
                        var command = "insert into reserved_rooms (ID_RESERVATION, ID_ROOM) " +
                                      $"values ('{id}', '{room}');";

                        using (var cmd = new MySqlCommand(command, con))
                        { 
                            result = cmd.ExecuteNonQuery();
                        }
                    }
                }

            }
            catch (Exception e)
            {
                MessageBox.Show($"Nie udało się dodać rezerwacji o danych {id}"+e);
            }
            MessageBox.Show(result == 0 ? "Nie udało się dodać rezerwacji" : "Pomyślnie dodano rezerwację");
            var charge = GetCharge(id);
            if (result != 0 && charge >=0)
            {
                MessageBox.Show("Kosz rezerwacji to: " + charge + " zl");
            }

        }

        private static string GetReservationId(string dateStart, string dateEnd, string idClient)
        {
            var id = string.Empty;
            var command = "select ID_RESERVATION " +
                          "from RESERVATIONS" +
                          $" where DATE_START = '{dateStart}' AND DATE_END = '{dateEnd}' AND ID_CLIENT = '{idClient}';";
            try
            {
                using (var con = new MySqlConnection(Properties.Settings.Default.ConnectionString))
                {
                    using (var cmd = new MySqlCommand(command, con))
                    {
                        con.Open();
                        var reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            id = reader.GetString(0);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Nie udało się odnaleźć id" + e);
            }
            return id;
        }

        private void EditReservation()
        {
            if (
                MessageBox.Show(_parent.Parent, "Czy na pewno chcesz edytować rezerwację?", "Potwierdzenie",
                MessageBoxButton.YesNo) == MessageBoxResult.No) return;
            if (!Validate()) return;

            if (!ExecuteEdit()) return;
            if (!RemovePreviousRooms(_id)) return;
            ExecuteAddReservedRooms(_id);

        }

        private bool ExecuteEdit()
        {
            var dateStart = ParseDate(DatePickerStart.SelectedDate.Value);
            var dateEnd = ParseDate(DatePickerEnd.SelectedDate.Value);
            var idClient = _pageShowClients.GetSelection();
            string command = "update reservations " +
                             $"set date_start = {dateStart}, date_end={dateEnd}, id_client={idClient}" +
                             $"where id_reservation = {_id};";
            try
            {
                using (var con = new MySqlConnection(Properties.Settings.Default.ConnectionString))
                {
                    using (var cmd = new MySqlCommand(command, con))
                    {
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Błąd połączenia " + e);
                return false;
            }
            return true;
        }


        private bool RemovePreviousRooms(string id)
        {
            try
            {
                var command = "delete from reserved_rooms " +
                              $"where id_reservation = {id};";

                using (var con = new MySqlConnection(Properties.Settings.Default.ConnectionString))
                {
                    using (var cmd = new MySqlCommand(command, con))
                    {
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(_parent.Parent, e.ToString(), "Error", MessageBoxButton.YesNo);
            }


            return true;
        }
        private void Save()
        {
            switch (_mode)
            {
                case Modes.Add:
                    AddReservation();
                    break;
                case Modes.Edit:
                    EditReservation();
                    break;
            }
        }

        private bool Validate()
        {
            bool validationOk = true;
            if (DatePickerEnd.SelectedDate == null)
            {
                DatePickerEnd.BorderBrush = Brushes.Red;
                validationOk = false;
            }
            else DatePickerEnd.BorderBrush = Brushes.Gray;
            if (DatePickerStart.SelectedDate == null)
            { 
                DatePickerStart.BorderBrush = Brushes.Red;
                validationOk = false;
            }
            else DatePickerStart.BorderBrush = Brushes.Gray;
            if (string.IsNullOrWhiteSpace(_pageShowClients.GetSelection()))
            {
                FramePickClient.BorderBrush = Brushes.Red;
                validationOk = false;
            }
            else FramePickClient.BorderBrush = Brushes.Gray;
            if (_pageShowRooms.GetSelection().Count == 0)
            {
                FramePickRooms.BorderBrush = Brushes.Red;
                validationOk = false;
            }
            else FramePickRooms.BorderBrush = Brushes.Gray;
            if (DatePickerEnd.SelectedDate != null && DatePickerStart.SelectedDate != null)
            {
                if (DatePickerStart.SelectedDate.Value > DatePickerEnd.SelectedDate.Value)
                {
                    DatePickerEnd.BorderBrush = Brushes.Red;
                    DatePickerStart.BorderBrush = Brushes.Red;
                    validationOk = false;
                }

            }

            if (validationOk)
            {
                validationOk = CheckRoomsOccupancy();
            }
           return validationOk;
        }

        private bool CheckRoomsOccupancy()
        {
            var dateStart = DatePickerStart.SelectedDate.Value;
            var dateEnd = DatePickerEnd.SelectedDate.Value;
            var roomsList = _pageShowRooms.GetSelection();
            var sb = new StringBuilder();
            foreach (var room in roomsList)
            {
                var number = CheckRoomOccupancy(room, dateStart, dateEnd);
                if(!string.IsNullOrWhiteSpace(number))
                    sb.Append(number + ", ");
            }
            if (sb.Length <= 0) return true;
            MessageBox.Show("Następujące pokoje są w wybranym terminie zajęte: " + Environment.NewLine +
                            "Uaktualnij kalendarza i spróbuj ponownie");
            return false;
        }

        private string CheckRoomOccupancy(string idRoom, DateTime start, DateTime end)
        {
            try
            {
                using (var con = new MySqlConnection(Properties.Settings.Default.ConnectionString))
                {
                    using (var cmd = new MySqlCommand("GetRoomOccupancyPeriods", con))
                    {
                        con.Open();
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@id", idRoom);
                        cmd.Parameters["@id"].Direction = ParameterDirection.Input;
                        var r = cmd.ExecuteReader();
                        while (r.Read())
                        {
                            var b = r.GetDateTime(0);
                            var e = r.GetDateTime(1);
                            if ((start > b && start < e) || (end > start && end <= e))
                                return idRoom;
                        } 
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex);
            }
            return string.Empty;
        }

        private void ButtonSave_OnClick(object sender, RoutedEventArgs e)
        {
            Save();
        }

        private void ButtonUpdate_OnClick(object sender, RoutedEventArgs e)
        {
           UpdateBlackoutPeriods();
        }

        private void UpdateBlackoutPeriods()
        {
            ClearDatePickers();
            foreach (var room in _pageShowRooms.GetSelection())
            {      
                BlackoutOccupiedPeriods(room);
            }
        }

        private void BlackoutOccupiedPeriods(string idRoom)
        {
            try
            {
                using (var con = new MySqlConnection(Properties.Settings.Default.ConnectionString))
                {
                    using (var cmd = new MySqlCommand("GetRoomOccupancyPeriods", con))
                    {
                        con.Open();
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@id", idRoom);
                        cmd.Parameters["@id"].Direction = ParameterDirection.Input;
                        var r = cmd.ExecuteReader();
                        while (r.Read())
                        {
                            var b = r.GetDateTime(0);
                            var e = r.GetDateTime(1);
                            try
                            {
                                DatePickerEnd.BlackoutDates.Add(new CalendarDateRange(b, e));
                                DatePickerStart.BlackoutDates.Add(new CalendarDateRange(b, e));
                            }
                            catch (Exception ex)
                            {
                                
                            }
                            
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex);
            }
        }
    }
}
