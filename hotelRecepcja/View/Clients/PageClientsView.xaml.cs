﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using hotelRecepcja.Clients;

namespace hotelRecepcja
{
    /// <summary>
    /// Interaction logic for PageClientsView.xaml
    /// </summary>
    public partial class PageClientsView : Page
    {
        private enum WindowModes { Show, Add }

        public MainWindow Parent;
        private WindowModes _mode;
        private PageShowClients _pageShowClients;
        private PageAddClient _pageAddClient;

        private List<RepeatButton> _listLeftMenuButtons;
        public PageClientsView(MainWindow parent)
        {
            Parent = parent;
            InitializeComponent();
            InitializePages();
            AssignButtons();

            _mode = WindowModes.Show;
            ButtonShowClients.Background = Application.Current.TryFindResource("BrushLeftMenuActive") as SolidColorBrush;
            FrameMainView.Navigate(_pageShowClients);
        }

        private void AssignButtons()
        {
            _listLeftMenuButtons = new List<RepeatButton>() { ButtonShowClients, ButtonAddClient };
        }
        private void InitializePages()
        {
            _pageShowClients = new PageShowClients(this);
            _pageAddClient = new PageAddClient(this);
        }

        public void Refresh()
        {
            UpdateLayout();
        }

        private void UpdateHighlight(WindowModes mode)
        {
            _listLeftMenuButtons[(int)_mode].Background = new SolidColorBrush(Colors.Transparent);
            _listLeftMenuButtons[(int)(_mode = mode)].Background = Application.Current.TryFindResource("BrushLeftMenuActive") as SolidColorBrush;
        }

        public void RunEditMode(string id)
        {
            _pageAddClient.Refresh();
            _pageAddClient.RunEditMode(id);
            FrameMainView.Navigate(_pageAddClient);
            UpdateHighlight(WindowModes.Add);
        }

        public void RunAddMode()
        {
            _pageAddClient.Refresh();
            _pageAddClient.RunAddMode();
            FrameMainView.Navigate(_pageAddClient);
            UpdateHighlight(WindowModes.Add);
        }

        private void ButtonShowClients_OnClick(object sender, RoutedEventArgs e)
        {
            if (_mode == WindowModes.Show) return;
            _pageShowClients.Refresh();
            FrameMainView.Navigate(_pageShowClients);
            UpdateHighlight(WindowModes.Show);
        }

        private void ButtonAddClient_OnClick(object sender, RoutedEventArgs e)
        {
            if (_mode == WindowModes.Add) return;
           RunAddMode();
        }
    }
}
