﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MySql.Data.MySqlClient;

namespace hotelRecepcja.Clients
{
    /// <summary>
    /// Interaction logic for PageAddClient.xaml
    /// </summary>
    public partial class PageAddClient : Page
    {
        private enum Modes
        {
            Add,
            Edit
        }

        private Modes _mode;
        private string _id;
        private PageClientsView _parent;
        public PageAddClient(PageClientsView parent)
        {
            _parent = parent;
            InitializeComponent();
        }

        public void Refresh()
        {
            TextBoxName.Text = string.Empty;
            TextBoxSurname.Text = String.Empty;
            TextBoxPhone.Text = String.Empty;
            TextBoxPhone.BorderBrush = TextBoxSurname.BorderBrush = TextBoxName.BorderBrush = new SolidColorBrush(Colors.Gray);
        }

        public void RunAddMode()
        {
            Refresh();
            _mode = Modes.Add;
        }

        public void RunEditMode(string id)
        {
            Refresh();
            _mode = Modes.Edit;
            _id = id;
            FillForm(id);
        }

        private void FillForm(string id)
        {
            using (var con = new MySqlConnection(Properties.Settings.Default.ConnectionString))
            {
                using (var cmd =new MySqlCommand($"select * from clients where id_client={id}", con))
                {
                    con.Open();
                    var reader = cmd.ExecuteReader();
                    if (!reader.Read()) return;
                    TextBoxName.Text = reader["FIRST_NAME"].ToString();
                    TextBoxSurname.Text = reader["LAST_NAME"].ToString();
                    TextBoxPhone.Text = reader["PHONE"].ToString();
                }
            }
        }

        private static bool ValidateStringTextBox(TextBox tb)
        {
            if (string.IsNullOrWhiteSpace(tb.Text))
            {
                tb.BorderBrush = Brushes.Red;
                return false;
            }
            tb.BorderBrush = Brushes.Gray;
            return true;
        }

        private static bool ValidateNumericTextBox(TextBox tb)
        {
            int n;
            bool isNum = int.TryParse(tb.Text, out n);
            if (!isNum)
            {
                tb.BorderBrush = new SolidColorBrush(Colors.Red);
                return false;
            }
            tb.BorderBrush = new SolidColorBrush(Colors.Gray);
            return true;
        }

        private void AddClient()
        {
            if (!ValidateStringTextBox(TextBoxName) ||
                !ValidateStringTextBox(TextBoxSurname) ||
                !ValidateNumericTextBox(TextBoxPhone))
            {
                MessageBox.Show("Dane nie sa poprawne, nie dodano klienta");
                return;
            }
            try
            {
                using (var con = new MySqlConnection(Properties.Settings.Default.ConnectionString))
                {
                    using (var cmd =new MySqlCommand("INSERT INTO CLIENTS(FIRST_NAME, LAST_NAME, PHONE) VALUES(@name, @surname, @phone)", con))
                    {
                        cmd.Parameters.AddWithValue("@name", TextBoxName.Text);
                        cmd.Parameters.AddWithValue("@surname", TextBoxSurname.Text);
                        cmd.Parameters.AddWithValue("@phone", TextBoxPhone.Text);
                        con.Open();
                        var result = cmd.ExecuteNonQuery();
                        MessageBox.Show(result == 0 ? "Nie udało się dodać klienta" : "Pomyślnie dodano klienta");
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Błąd połączenia " + e);
            }

        }

        private void EditClient()
        {
            if (!ValidateStringTextBox(TextBoxName) ||
                !ValidateStringTextBox(TextBoxSurname) ||
                !ValidateNumericTextBox(TextBoxPhone))
            {
                MessageBox.Show("Dane nie sa poprawne, nie dodano klienta");
                return;
            }
            
            try
            {
                using (var con = new MySqlConnection(Properties.Settings.Default.ConnectionString))
                {
                    using ( var cmd = new MySqlCommand("UPDATE CLIENTS SET FIRST_NAME=@name, LAST_NAME=@surname, PHONE=@phone where ID_CLIENT=@id", con))
                    {
                        cmd.Parameters.AddWithValue("@name", TextBoxName.Text);
                        cmd.Parameters.AddWithValue("@surname", TextBoxSurname.Text);
                        cmd.Parameters.AddWithValue("@phone", TextBoxPhone.Text);
                        cmd.Parameters.AddWithValue("@id", _id);
                        con.Open();
                        var result = cmd.ExecuteNonQuery();
                        MessageBox.Show(result == 0 ? "Nie udało się edytować klienta" : "Pomyślnie edytowano klienta");
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Błąd połączenia " + e);
            }
        }

        private void ValidationString(object sender, TextChangedEventArgs e)
        {
            var tb = sender as TextBox;
            if (tb == null) return;
            ValidateStringTextBox(tb);
        }

        private void ValidationNumeric(object sender, TextChangedEventArgs e)
        {
            var tb = sender as TextBox;
            if (tb == null) return;
            ValidateNumericTextBox(tb);
        }

        private void ButtonAddClient_OnClick(object sender, RoutedEventArgs e)
        {
            switch (_mode)
            {
                case Modes.Add:
                    AddClient();
                    break;
                case Modes.Edit:
                    EditClient();
                    break;
            }
        }
    }
}
