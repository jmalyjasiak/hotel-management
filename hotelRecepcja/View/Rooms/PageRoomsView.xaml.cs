﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using hotelRecepcja.Clients;
using hotelRecepcja.Rooms;

namespace hotelRecepcja
{
    /// <summary>
    /// Interaction logic for PageRoomsView.xaml
    /// </summary>
    public partial class PageRoomsView : Page
    {
        private enum WindowModes { Show, Add }

        public MainWindow _parent;
        private WindowModes _mode;
        private PageShowRooms _pageShowRooms;
        private PageAddRoom _pageAddRoom;
        private List<RepeatButton> _listLeftMenuButtons;
        public PageRoomsView(MainWindow parent)
        {
            _parent = parent;
            InitializeComponent();
            InitializePages();
            AssignButtons();
            _mode = WindowModes.Show;
            FrameMainView.NavigationService.Navigate(_pageShowRooms);
        }

        private void InitializePages()
        {
            _pageShowRooms = new PageShowRooms(this, false);
            _pageAddRoom = new PageAddRoom(this);
        }

        private void AssignButtons()
        {
            _listLeftMenuButtons = new List<RepeatButton>() { ButtonShowRooms, ButtonAddRoom };
        }

        public void Refresh()
        {
            UpdateLayout();
        }

        private void UpdateHighligh(WindowModes mode)
        {
            _listLeftMenuButtons[(int)_mode].Background = new SolidColorBrush(Colors.Transparent);
            _listLeftMenuButtons[(int)(_mode = mode)].Background = Application.Current.TryFindResource("BrushLeftMenuActive") as SolidColorBrush;
        }

        public void RunEditMode(int id)
        {
            UpdateHighligh(WindowModes.Add);
            _pageAddRoom.Refresh();
            _pageAddRoom.RunEditMode(id);
            FrameMainView.Navigate(_pageAddRoom);
        }

        private void ButtonShowRooms_OnClick(object sender, RoutedEventArgs e)
        {
            if (_mode == WindowModes.Show) return;
            UpdateHighligh(WindowModes.Show);
            _pageShowRooms.Refresh();
            FrameMainView.Navigate(_pageShowRooms);
        }

        private void ButtonAddRoom_OnClick(object sender, RoutedEventArgs e)
        {
            if (_mode == WindowModes.Add) return;
            UpdateHighligh(WindowModes.Add);
            _pageAddRoom.Refresh();
            FrameMainView.Navigate(_pageAddRoom);
        }
    }
}
